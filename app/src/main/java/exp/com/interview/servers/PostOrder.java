package exp.com.interview.servers;

import android.content.Context;
import android.os.AsyncTask;
import android.text.format.DateUtils;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by indglobal on 7/12/2017.
 */

public class PostOrder extends AsyncTask<String, Void, String>

{
    String result = "null";
    JSONObject jsonObject;
    Context context;


    public PostOrder(Context context, JSONObject jsonObject) {
        this.context=context;
        this.jsonObject=jsonObject;

    }
    protected void onPreExecute()
    {
        //progressDialog = ProgressDialog.show(context, "", "Processing...", false);
        //  progressDialog = new ProgressDialog(context, R.style.MyTheme);
//        progressDialog.setCancelable(false);
        //  progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        //  progressDialog.show();
    }
    @Override
    protected String doInBackground(String... params) {
        String url=params[0];
        return make_aserver_call(url);
    }

    protected void onPostExecute(String result) {
        //   progressDialog.dismiss();
        System.out.println("******************* Result *********************" + result);
        if(!result.equalsIgnoreCase("null") && !result.isEmpty())
        {
            if(!result.equalsIgnoreCase("No Response"))
            {
                boolean jsonTest=isJSONValid(result);
                if(jsonTest==true)
                {
                    try {
                        JSONObject jsonObject = new JSONObject(result);


                       /* String orderid= String.valueOf(Cart_Fragment.orderid);

                        OrdersAdapter.db.execSQL("UPDATE Orders SET sync='1' WHERE id= " + orderid);
*/
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                }else{
                    Toast.makeText(context, "Slow Internet,Please try after sometime",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(context, "Slow Internet,Please try after sometime",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(context, "Slow Internet,Please try after sometime",Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    private String make_aserver_call(String url) {

        StringBuilder sb=new StringBuilder();
        DataOutputStream outputstream;
        int stauscode=0;
        int some_reasonable_timeout = (int) (3 * DateUtils.SECOND_IN_MILLIS);
        try {
            URL Url=new URL(url);
            try {
                HttpURLConnection urlConnection= (HttpURLConnection) Url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setConnectTimeout(some_reasonable_timeout);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                outputstream=new DataOutputStream(urlConnection.getOutputStream());
                outputstream.writeBytes(jsonObject.toString());
                outputstream.flush();
                outputstream.close();
                urlConnection.connect();
                //Get Response
                stauscode=urlConnection.getResponseCode();
                if(stauscode== HttpURLConnection.HTTP_OK)
                {
                    InputStream responseStream = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();
                    String response = stringBuilder.toString();
                    result=response;
                }
                else
                {
                    result= "No Response";
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return result;
    }
}

