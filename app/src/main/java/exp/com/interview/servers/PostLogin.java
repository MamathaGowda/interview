package exp.com.interview.servers;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import exp.com.interview.R;
import exp.com.interview.activities.EnrolmentActivity;
import exp.com.interview.activities.MainActivity;
import exp.com.interview.adapters.LoginDataBaseAdapter;
import exp.com.interview.commons.Comman;


/**
 * Created by pc on 7/11/2017.
 */

public class PostLogin extends AsyncTask<String, Void, String> {
    Context context;
    String name,hash,noHash, username;
    LoginDataBaseAdapter loginDataBaseAdapter;
    String login_id;

    public PostLogin(Context context, String name, String hash, String noHash) {
        this.context = context;
        this.name = name;
        this.hash = hash;
        this.noHash = noHash;
    }

    @Override
    protected void onPreExecute() {
        //  processing = true;
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            String response="";
            String handlerUrl = context.getResources().getString(R.string.postloginUrl);
            URL url = new URL(handlerUrl);
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(handlerUrl);

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
            nameValuePairs.add(new BasicNameValuePair("username", name));
            nameValuePairs.add(new BasicNameValuePair("password", hash));
            nameValuePairs.add(new BasicNameValuePair("noHash", noHash));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse res = client.execute(post);
            HttpEntity resEntity = res.getEntity();
            final String response_str = EntityUtils.toString(resEntity);
            return response_str;

            //------------------>>
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String result) {

        try {
            if (result != null) {
                JSONObject jsonObject = new JSONObject(result);
                String reshash = jsonObject.getString("hash");
                String resuname = jsonObject.getString("firstname");

                JSONObject jsonObject1 = jsonObject.getJSONObject("user");

                login_id= Comman.getPreferences(context, "login_id");

                if(login_id.equalsIgnoreCase("1")){
                    MainActivity.prgLoading.setVisibility(View.GONE);
                    Intent intent = new Intent(context, EnrolmentActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                else {
                    username = "assignment";

                    loginDataBaseAdapter=new LoginDataBaseAdapter(context);
                    loginDataBaseAdapter=loginDataBaseAdapter.open();

                    loginDataBaseAdapter=new LoginDataBaseAdapter(context);
                    loginDataBaseAdapter=loginDataBaseAdapter.open();
                    loginDataBaseAdapter.updateEntry(name,reshash);
                    Comman.setPreferences(context,"login_id","1");
                    MainActivity.prgLoading.setVisibility(View.GONE);
                }

            } else {
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }
}


