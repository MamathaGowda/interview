package exp.com.interview.servers;

import android.content.Context;
import android.os.AsyncTask;
import android.text.format.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import exp.com.interview.beans.Enrolment;

/**
 * Created by pc on 7/11/2017.
 */

public class GetEnrolment extends AsyncTask<String,Void,String> {
    Context context;

    public static String result = "null";
    ArrayList<Enrolment> enrolmentArrayList = new ArrayList<>();
    Enrolment enrolment;


    public GetEnrolment(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        String url=params[0];
        return make_aserver_call(url);
    }

    protected void onPostExecute(String result) {
        //progressDialog.dismiss();

        System.out.println(" Result **" + result);

        if (!result.equalsIgnoreCase("null") && !result.isEmpty()) {
            if (!result.equalsIgnoreCase("No Response")) {
                boolean jsonTest = isJSONValid(result);
                if (jsonTest == true) {

                    try{
                        JSONObject jsonobj  = new JSONObject(result);
                        JSONArray jsonArray = jsonobj.getJSONArray("tableWsList");

                        enrolment = new Enrolment();
                        enrolment.setName("name");
                        enrolment.setCustom("custom");
                        enrolment.setLanguage("english");
                        enrolment.setSource("source");
                        enrolment.setType("type");
                        enrolmentArrayList.add(enrolment);


                        //  Tab_Fragment.int_items=MenuList.size();
                    }catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            }
        }
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }



    private String make_aserver_call(String url) {

        StringBuilder sb =new StringBuilder();
        DataOutputStream outputstream;
        int statuscode=0;
        int some_reasonable_timeout = (int) (3 * DateUtils.SECOND_IN_MILLIS);//Internet check for 3 second
        try {
            URL Url=new URL(url);
            try {
                HttpURLConnection urlConnection= (HttpURLConnection) Url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setConnectTimeout(some_reasonable_timeout);
                statuscode=urlConnection.getResponseCode();

                if(statuscode== HttpURLConnection.HTTP_OK)
                {
                    InputStream responseStream = new BufferedInputStream(urlConnection.getInputStream());
                    BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
                    String line = "";
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = responseStreamReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }
                    responseStreamReader.close();
                    String response = stringBuilder.toString();
                    result=response;
                }
                else
                {
                    result= "No Response";
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return result;
    }
}

