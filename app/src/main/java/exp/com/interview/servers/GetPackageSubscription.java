package exp.com.interview.servers;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import exp.com.interview.R;
import exp.com.interview.activities.EnrolmentActivity;
import exp.com.interview.adapters.EnrolmentAdapter;
import exp.com.interview.beans.Enrolment;

/**
 * Created by android2 on 03/03/17.
 */

public class GetPackageSubscription extends AsyncTask<String, Void, String> {

    Context context;
    ArrayList<Enrolment> enrolmentArrayList = new ArrayList<>();
    Enrolment enrolment;
    EnrolmentAdapter enrolmentAdapter;

    public GetPackageSubscription(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        try {

            String productListUrl = context.getResources().getString(R.string.getenrolmentUrl) + "username=" + "assignment" + "&" + "password=" + "1";
            productListUrl = productListUrl.replaceAll("\\s+", "%20");
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(productListUrl);

            httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials("assignment", "1"), "UTF-8", false));

            // Set up the header types needed to properly transfer JSON
            httpGet.setHeader("Content-Type", "application/json");

            HttpResponse response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            final String response_str = EntityUtils.toString(entity);

            return response_str;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onPostExecute(String result) {
        try {
            if (result != null) {
                JSONArray jsonArray = new JSONArray(result);
                for (int i=0;i<jsonArray.length();i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    enrolment = new Enrolment();
                    if(jsonObject.getString("id").equalsIgnoreCase(""))
                    {
                        enrolment.setId(" ");
                    }
                    else {
                        enrolment.setId(jsonObject.getString("_id"));
                    }
                    if(jsonObject.getString("code").equalsIgnoreCase(""))
                    {
                        enrolment.setCode(jsonObject.getString(""));
                    }
                    else {
                        enrolment.setCode(jsonObject.getString("code"));
                    }
                    if(jsonObject.getString("type").equalsIgnoreCase(""))
                    {
                        enrolment.setType("");
                    }
                    else {
                        enrolment.setType(jsonObject.getString("type"));
                    }if(jsonObject.getString("id").equalsIgnoreCase(""))
                {

                }
                else {

                }
                    if(jsonObject.getString("id").equalsIgnoreCase(""))
                    {

                    }
                    else {

                    } if(jsonObject.getString("id").equalsIgnoreCase(""))
                {

                }
                else {

                }
                

                    enrolment.setYear(jsonObject.getString("year"));
                    enrolment.setName(jsonObject.getString("name"));
                    enrolment.setSubject(jsonObject.getString("subject"));
                  //  enrolment.setGrade(jsonObject.getJSONArray("grade").getString(0));
                    enrolment.setLanguage(jsonObject.getString("language"));
                    enrolment.setCreated(jsonObject.getString("created"));
                    enrolment.setPrivate1(jsonObject.getString("private"));
                    enrolment.setOwner(jsonObject.getString("owner"));
                    enrolment.setCustom(jsonObject.getString("custom"));
                    enrolment.setSource(jsonObject.getString("source"));
                    enrolment.setUpdated(jsonObject.getString("updated"));
                    enrolment.setUpdatedBy(jsonObject.getString("updatedBy"));
                    enrolmentArrayList.add(enrolment);
                }
                if(enrolmentArrayList.size()>0)
                {
                    enrolmentAdapter = new EnrolmentAdapter(context,enrolmentArrayList);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                    EnrolmentActivity.rv_enrolment.setLayoutManager(mLayoutManager);
                    EnrolmentActivity.rv_enrolment.setAdapter(enrolmentAdapter);
                    enrolmentAdapter.notifyDataSetChanged();
                    EnrolmentActivity.swipeRefreshLayout.setRefreshing(false);
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


