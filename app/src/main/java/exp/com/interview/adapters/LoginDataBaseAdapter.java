package exp.com.interview.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import exp.com.interview.sqlite.DataBaseHelper;

/**
 * Created by pc on 7/12/2017.
 */

public class LoginDataBaseAdapter {

    static final String DATABASE_NAME = "Interview.db";
    static final int DATABASE_VERSION = 1;
    public static final String NAME_TABLE= "LOGIN";

    public static final String DATABASE_CREATE = "create table " + NAME_TABLE +
            "( " + "ID" + " integer primary key autoincrement," + "USERNAME text,HASH text); ";

    public SQLiteDatabase db;
    private final Context context;
    private DataBaseHelper dbHelper;

    public LoginDataBaseAdapter(Context _context) {
        context = _context;
        dbHelper = new DataBaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public LoginDataBaseAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        db.close();
    }

    public SQLiteDatabase getDatabaseInstance() {
        return db;
    }

    public void insertEntry(String userName, String hash) {
        ContentValues newValues = new ContentValues();
       newValues.put("USERNAME", userName);
        newValues.put("HASH", hash);

       db.insert(NAME_TABLE, null, newValues);
      Toast.makeText(context, "Username and hash Is Successfully Saved", Toast.LENGTH_LONG).show();
    }

    public int deleteEntry(String UserName) {
        String where = "USERNAME=?";
        int numberOFEntriesDeleted = db.delete(NAME_TABLE, where, new String[]{UserName});
        Toast.makeText(context, "Number fo Entry Deleted Successfully : "+numberOFEntriesDeleted, Toast.LENGTH_LONG).show();
        return numberOFEntriesDeleted;
    }

    public String getSinlgeEntry(String userName) {
        Cursor cursor = db.query(NAME_TABLE, null, " USERNAME=?", new String[]{userName}, null, null, null);
        if (cursor.getCount() < 1) // UserName Not Exist
        {
            cursor.close();
            return "NOT EXIST";
        }
        cursor.moveToFirst();
        String hash = cursor.getString(cursor.getColumnIndex("HASH"));
        cursor.close();
        return hash;
    }


    public void updateEntry(String userName, String hash) {
        ContentValues updatedValues = new ContentValues();
        updatedValues.put("USERNAME", userName);
        updatedValues.put("HASH", hash);

        String where = "USERNAME = ?";
        db.update(NAME_TABLE, updatedValues, where, new String[]{userName});
    }
}