package exp.com.interview.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import exp.com.interview.R;
import exp.com.interview.beans.Enrolment;

/**
 * Created by indglobal on 7/12/2017.
 */

public class EnrolmentAdapter extends RecyclerView.Adapter<EnrolmentAdapter.MyViewHolder> {
    Context context;
    ArrayList<Enrolment> enrolmentArrayList = new ArrayList<>();

    public EnrolmentAdapter(Context context, ArrayList<Enrolment> enrolmentArrayList) {
        this.context = context;
        this.enrolmentArrayList = enrolmentArrayList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_lang,tv_source,tv_custom,tv_type;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_lang = (TextView) itemView.findViewById(R.id.tv_lang);
            tv_source = (TextView) itemView.findViewById(R.id.tv_source);
            tv_custom = (TextView) itemView.findViewById(R.id.tv_custom);
            tv_type = (TextView) itemView.findViewById(R.id.tv_type);
        }
    }
    @Override
    public EnrolmentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EnrolmentAdapter.MyViewHolder holder, int position) {

        holder.tv_name.setText(enrolmentArrayList.get(position).getName());
        holder.tv_lang.setText(enrolmentArrayList.get(position).getLanguage());
        holder.tv_source.setText(enrolmentArrayList.get(position).getSource());
        holder.tv_custom.setText(enrolmentArrayList.get(position).getCustom());
        holder.tv_type.setText(enrolmentArrayList.get(position).getType());
    }

    @Override
    public int getItemCount() {
        return enrolmentArrayList.size();
    }
}
