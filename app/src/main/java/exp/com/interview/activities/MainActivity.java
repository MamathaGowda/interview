package exp.com.interview.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import exp.com.interview.R;
import exp.com.interview.adapters.LoginDataBaseAdapter;
import exp.com.interview.commons.Comman;
import exp.com.interview.servers.PostLogin;


public class MainActivity extends Activity implements View.OnClickListener{

    public static ProgressBar prgLoading;
    EditText etLoginUname,etLoginHash;
    LinearLayout llLogin;

    String username,hash;
    boolean processing = false;
    public static boolean firsttime;

    LoginDataBaseAdapter loginDataBaseAdapter;
    String login_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hideKeyboard();

        prgLoading = (ProgressBar) findViewById(R.id.progressBar);
        llLogin = (LinearLayout) findViewById(R.id.llLogin);
        etLoginUname = (EditText)findViewById(R.id.etLoginUname);
        etLoginHash = (EditText)findViewById(R.id.etLoginHash);

        llLogin.setOnClickListener(this);
        login_id= Comman.getPreferences(MainActivity.this, "login_id");

        if (login_id.equalsIgnoreCase("1"))
        {
            hideKeyboard();
            prgLoading.setVisibility(View.VISIBLE);
            username = "assignment";

            loginDataBaseAdapter=new LoginDataBaseAdapter(this);
            loginDataBaseAdapter=loginDataBaseAdapter.open();
            String hash1 = loginDataBaseAdapter.getSinlgeEntry(username);

            try {
               new PostLogin(getApplicationContext(),username,hash1,"1").execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
// TODO Auto-generated method stub
        super.onDestroy();
        loginDataBaseAdapter.close();
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        switch (id){
            case R.id.llLogin:
                if (!processing){
                    if(!login_id.equalsIgnoreCase("1")){
                        hideKeyboard();
                        username = etLoginUname.getText().toString();
                        hash = etLoginHash.getText().toString();

                        if (!Comman.isConnectionAvailable(MainActivity.this)){
                            Toast.makeText(MainActivity.this,"Please check your internet connection!",Toast.LENGTH_SHORT).show();
                        }else if (username.equalsIgnoreCase("")){
                            Toast.makeText(MainActivity.this,"Please enter a user name!",Toast.LENGTH_SHORT).show();
                        }else if (hash.equalsIgnoreCase("")){
                            Toast.makeText(MainActivity.this,"Please provide a hash!",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            prgLoading.setVisibility(View.VISIBLE);
                            username = "assignment";

                            loginDataBaseAdapter=new LoginDataBaseAdapter(getApplicationContext());
                            loginDataBaseAdapter=loginDataBaseAdapter.open();
                            loginDataBaseAdapter.insertEntry(username,hash);
                            prgLoading.setVisibility(View.GONE);
                            try {
                                new PostLogin(getApplicationContext(),username,hash,"0").execute();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            etLoginUname.setText("");
                            etLoginHash.setText("");
                        }
                    }
                }else {
                    Toast.makeText(MainActivity.this,"Please wait a moment!",Toast.LENGTH_SHORT).show();
                }
        }
    }
    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
