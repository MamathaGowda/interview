package exp.com.interview.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import exp.com.interview.R;
import exp.com.interview.adapters.EnrolmentAdapter;
import exp.com.interview.beans.Enrolment;
import exp.com.interview.servers.GetEnrolment;
import exp.com.interview.servers.GetPackageSubscription;

public class EnrolmentActivity extends Activity {

    public static SwipeRefreshLayout swipeRefreshLayout;
    public static RecyclerView rv_enrolment;

    EnrolmentAdapter enrolmentAdapter;
    ArrayList<Enrolment> enrolmentArrayList = new ArrayList<>();
    Enrolment enrolment;
    String productListUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enrolment);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefresh);
        rv_enrolment = (RecyclerView) findViewById(R.id.rv);

        productListUrl = getResources().getString(R.string.getenrolmentUrl) + "username=" + "assignment" + "&" + "password=" + "1";

        new GetPackageSubscription(getApplicationContext()).execute();

      //  new GetEnrolment(getApplicationContext()).execute(productListUrl);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetEnrolment(getApplicationContext()).execute(productListUrl);
            }
        });


    }
}
